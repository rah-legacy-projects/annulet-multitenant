/*
Multi-tenancy for Mongoose

See readme for examples and info
@author Jason Raede <jason@torchedm.com>
*/

var dot, mongoose, owl, _;

mongoose = require('mongoose');

dot = require('dot-component');

_ = require('underscore');

owl = require('owl-deepcopy');

/*
Added by @watnotte
*/


module.exports = {
    collectionDelimiter: '__',
    setDelimiter: function(delimiter) {
        if (delimiter) {
            return this.collectionDelimiter = delimiter;
        }
    },
    setup: function(connection) {
        var collectionDelimiter;
        connection = connection || mongoose;
        collectionDelimiter = this.collectionDelimiter;
        connection.mtModel = function(name, schema, collectionName) {
            var extendPathWithTenantId, extendSchemaWithTenantId, make, modelName, multitenantSchemaPlugin, parts, precompile, tenantId;
            precompile = [];
            extendPathWithTenantId = function(tenantId, path) {
                var key, newPath, val, _ref;
                if (path.instance !== 'ObjectID' && path.instance !== mongoose.Schema.Types.ObjectId) {
                    return false;
                }
                if ((path.options.$tenant == null) || path.options.$tenant !== true) {
                    return false;
                }
                newPath = {
                    type: mongoose.Schema.Types.ObjectId
                };
                _ref = path.options;
                for (key in _ref) {
                    val = _ref[key];
                    if (key !== 'type') {
                        newPath[key] = _.clone(val, true);
                    }
                }
                newPath.ref = tenantId + collectionDelimiter + path.options.ref;
                precompile.push(tenantId + '.' + path.options.ref);
                return newPath;
            };
            extendSchemaWithTenantId = function(tenantId, schema) {
                var config, extension, newPath, newSchema, newSubSchema, prop, _ref;
                extension = {};
                newSchema = owl.deepCopy(schema);
                newSchema.callQueue.forEach(function(k) {
                    var args, key, val, _ref, _results;
                    args = [];
                    _ref = k[1];
                    _results = [];
                    for (key in _ref) {
                        val = _ref[key];
                        args.push(val);
                        _results.push(k[1] = args);
                    }
                    return _results;
                });
                _ref = schema.paths;
                for (prop in _ref) {
                    config = _ref[prop];
                    if (config.options.type instanceof Array) {
                        if (config.schema != null) {
                            newSubSchema = extendSchemaWithTenantId(tenantId, config.schema);
                            newSubSchema = extendSchemaWithTenantId(tenantId, config.schema);
                            newSchema.path(prop, [newSubSchema]);
                        } else {
                            newPath = extendPathWithTenantId(tenantId, config.caster);
                            if (newPath) {
                                newSchema.path(prop, [newPath]);
                            }
                        }
                    } else {
                        if (config.schema != null) {
                            newSubSchema = extendSchemaWithTenantId(tenantId, config.schema);
                            newSchema.path(prop, newSubSchema);
                        } else {
                            newPath = extendPathWithTenantId(tenantId, config);
                            if (newPath) {
                                newSchema.path(prop, newPath);
                            }
                        }
                    }
                }
                return newSchema;
            };
            multitenantSchemaPlugin = function(schema, options) {
                schema.statics.getTenantId = schema.methods.getTenantId = function() {
                    return this.schema.$tenantId;
                };
                return schema.statics.getModel = schema.methods.getModel = function(name) {
                    return connection.mtModel(this.getTenantId() + '.' + name);
                };
            };
            make = function(tenantId, modelName) {
                var model, newSchema, origSchema, pre, preModelName, tenantCollectionName, tenantModelName, uniq, _i, _len;
                tenantModelName = tenantId + collectionDelimiter + modelName;
                if (connection.models[tenantModelName] != null) {
                    return connection.models[tenantModelName];
                }
                model = this.model(modelName);
                tenantCollectionName = tenantId + collectionDelimiter + model.collection.name;
                origSchema = model.schema;
                newSchema = extendSchemaWithTenantId(tenantId, origSchema);
                newSchema.$tenantId = tenantId;
                newSchema.plugin(multitenantSchemaPlugin);
                if (connection.mtModel.goingToCompile.indexOf(tenantModelName) < 0) {
                    connection.mtModel.goingToCompile.push(tenantModelName);
                }
                if (precompile.length) {
                    uniq = _.uniq(precompile);
                    for (_i = 0, _len = uniq.length; _i < _len; _i++) {
                        pre = uniq[_i];
                        pre = pre.slice(tenantId.length + 1);
                        preModelName = tenantId + collectionDelimiter + pre;
                        if ((connection.models[preModelName] == null) && mongoose.mtModel.goingToCompile.indexOf(preModelName) < 0) {
                            connection.mtModel(tenantId, pre);
                        }
                    }
                }
                return this.model(tenantModelName, newSchema, tenantCollectionName);
            };
            if (arguments.length === 1) {
                parts = arguments[0].split('.');
                modelName = parts.pop();
                tenantId = parts.join('.');
                return make.call(this, tenantId, modelName);
            } else if (arguments.length === 2) {
                if (arguments[1] instanceof mongoose.Schema) {
                    return this.model(arguments[0], arguments[1]);
                } else {
                    return make.call(this, arguments[0], arguments[1]);
                }
            } else if (aguments.length === 3) {
                return this.model(arguments[0], arguments[1], arguments[2]);
            } else {
                throw new Error('invalid arguments');
            }
        };
        return connection.mtModel.goingToCompile = [];
    }
};
